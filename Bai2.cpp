//
//  main.cpp
//  THLTNNCLab2
//
//  Created by Tran Ngoc Linh on 4/4/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
#include <cstring>
using namespace std;
int main(int argc, const char * argv[]) {
    string str;
    int count = 0;
    getline(cin,str);
    
    if (str[0]!=' ') {
        count++;
    }
    for (int i=1; i<str.length(); i++) {
        if(str[i]!=' '&&str[i-1]==' ') count++;
    }
    
    cout<<"Number of words is: "<<count;
    return 0;
}
