//
//  main.cpp
//  THLTNNCLab2
//
//  Created by Tran Ngoc Linh on 4/4/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
#include <cstring>
#include <math.h>
using namespace std;
int main(int argc, const char * argv[]) {
    float money;
    int month;
    float interest;
    float sumMoney;
    cout<<"Money for save: ";
    cin>>money;
    cout<<"Month: ";
    cin>>month;
    sumMoney = money*pow(1+7.0/100/12, month);
    cout<<"Sum of money after "<<month<<" is: "<<sumMoney;
    return 0;
}
