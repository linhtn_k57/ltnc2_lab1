//
//  main.cpp
//  THLTNNCLab2
//
//  Created by Tran Ngoc Linh on 4/4/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
#include <cstring>
#include <math.h>
using namespace std;
int main(int argc, const char * argv[]) {
    int count = 0;
    int a[100];
    int n;
    cout<<"Enter n: ";
    cin>>n;
    cout<<"Enter array:\n";
    for (int i = 0; i<n; i++) {
        cin>>a[i];
    }
    
    for (int i=1; i<n; i++) {
        for (int j=0; j<i; j++) {
            if((a[i]>a[j]&&a[i]%a[j]==0)||(a[j]>a[i]&&a[j]%a[i]==0)) count++;
        }
    }
    cout<<"Number of pair: "<<count<<endl;
    
    return 0;
}
